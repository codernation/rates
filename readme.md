# How to build and start application

### Reference Documentation
1. Run command to build application: 
* mvn install
2. Run command to start application: 
* java -jar target/rates-0.0.1-SNAPSHOT.jar
3. Check for rates:
* http://localhost:8080/rate?code=EUR&date=2014-01-01 12:00:00