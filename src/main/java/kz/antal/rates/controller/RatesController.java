package kz.antal.rates.controller;

import kz.antal.rates.model.CurrencyCode;
import kz.antal.rates.model.CurrencyRates;
import kz.antal.rates.repository.CurrencyRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Olzhas.Pazyldayev on 02.06.2019
 */

@RestController
public class RatesController {

    @Autowired
    CurrencyRateRepository repository;

    @RequestMapping(value = "/rate", method = RequestMethod.GET)
    public BigDecimal query(@RequestParam CurrencyCode code,
                            @RequestParam String date) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        if (code.equals(CurrencyCode.KZT)) {
            return BigDecimal.ONE;
        }

        CurrencyRates rate = repository.findBySrcCurrencyAndLastUpdate(code, dateTime);
        if (rate != null) {
            return rate.getRate();
        } else {
            throw new RuntimeException("rate not found!");
        }
    }
}