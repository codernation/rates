package kz.antal.rates.model;

/**
 * @author by Olzhas.Pazyldayev on 02.06.2019.
 */
public enum CurrencyCode {
    KZT,
    USD,
    EUR;

    public static CurrencyCode getCurrencyCode(String currencyCode) {
        return valueOf(currencyCode);
    }

}
