package kz.antal.rates.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author by Olzhas.Pazyldayev on 02.06.2019.
 */
@Getter
@Setter
@Entity
@Table(name = "currency_rates")
public class CurrencyRates {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "srccurrency")
    private CurrencyCode srcCurrency;

    @Enumerated(EnumType.STRING)
    @Column(name = "dstcurrency")
    private CurrencyCode dstCurrency;

    @Column(name = "lastupdate")
    private LocalDateTime lastUpdate;

    @Column(name = "rate")
    private BigDecimal rate;

    public CurrencyRates() {
    }
}
