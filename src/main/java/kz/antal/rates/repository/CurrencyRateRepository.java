package kz.antal.rates.repository;

import kz.antal.rates.model.CurrencyCode;
import kz.antal.rates.model.CurrencyRates;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;

/**
 * @author Olzhas.Pazyldayev on 02.06.2019
 */
public interface CurrencyRateRepository extends CrudRepository<CurrencyRates, Long> {

    CurrencyRates findBySrcCurrencyAndLastUpdate(CurrencyCode code, LocalDateTime lastUpdate);

}
