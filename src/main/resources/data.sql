DROP TABLE IF EXISTS currency_rates;

create table currency_rates
(
    id INT not null,
    srcCurrency VARCHAR(3) not null,
    dstCurrency VARCHAR(3) not null,
    lastUpdate timestamp default CURRENT_TIMESTAMP,
    rate numeric(18, 4) not null
);
INSERT INTO currency_rates (id, srcCurrency, dstCurrency, lastUpdate, rate)
VALUES (1, 'USD', 'KZT', '2014-01-01 12:00:00', 150);
INSERT INTO currency_rates (id, srcCurrency, dstCurrency, lastUpdate, rate)
VALUES (1, 'EUR', 'KZT', '2014-01-01 12:00:00', 170);
INSERT INTO currency_rates (id, srcCurrency, dstCurrency, lastUpdate, rate)
VALUES (1, 'USD', 'KZT', '2019-01-01 12:00:00', 380);
INSERT INTO currency_rates (id, srcCurrency, dstCurrency, lastUpdate, rate)
VALUES (1, 'EUR', 'KZT', '2019-01-01 12:00:00', 420);